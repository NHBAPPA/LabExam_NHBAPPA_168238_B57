<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Task 2</title>
</head>
<body>
<script>
    var myarray=[78,60,62,68,71,68,71,30,68,73,85,66,64,76,63,75,76,73,68,62,73,72,65,74,65,64,68,75,79,73];
    function average(array) {
        var total= 0;
        for(var i=0;i<array.length;i++){
            total= total +array[i];
        }
        var aver = total / array.length;
        return aver;
    }
    document.write("Average Temperatures is: " + "["+average(myarray)+"]"+"<br>");
//document.write("<br>");

    //Sorting five lowest temperature
    var myarray = myarray.sort();
    var unique = Array.from(new  Set(myarray));
    document.write("<br>");
    document.write("List of five lowest temperature: [");
    for(var k=0;k<=4;k++) {
        if (k < 4) {
            document.write(' ' + unique[k] + ',' + ' ');
        }
        else {
            document.write(' ' + unique[k] + ' ')
        }
    }
        document.write("]");

    //Sorting five highest temperature
    myarray = myarray.reverse();
    document.write("<br>");
    document.write("<br>");
    document.write("List of five highest temperature: [");

    for(var k=0;k<=4;k++) {
        if (k < 4) {
            document.write(' ' + unique[k] + ',' + ' ');
        }
        else {
            document.write(' ' + unique[k] + ' ')
        }
    }
    document.write("]");

</script>
</body>
</html>

