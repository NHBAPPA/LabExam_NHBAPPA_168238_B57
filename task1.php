<!DOCTYPE HTML>
<html lang="en">
<head>
    <title>Task 1</title>

</head>
<body>

<script>
    var givenArray = [23, 6, 6,2,4,[2, 7,7,[6,4,5,6, 2, 1, 2],5,8, 2], 5,4,5,3, 12];
    var level = 0;

    function printSpaces() {

        for (var i=1; i<=8*level; i++){
            document.write("&nbsp;");
        }
    }
    function printArray(currentArray) {

        for(var i=0;i<currentArray.length;i++){

            if(Array.isArray(currentArray[i])){
                level++;
                printArray(currentArray[i]);
            }
            else{

                printSpaces();
                document.write(currentArray[i] +"<br>");
            }

        }
        level--;
    }

    printArray(givenArray);


</script>


</body>
</html>